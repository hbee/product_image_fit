Proper product image fit
==============================================================================
By default OpenERP scales product images by cropping parts which doesn't fit
into certain dimensions. This sometimes leads to malformed image, e.g. when you
have *narrow* image then top and bottom is chopped off leaving just image
center:

.. image:: https://bitbucket.org/hacbee/product_image_fit/raw/bb402ff75a01313d0a5c9d8b93edd3b359fdf7eb/src/img/scale_example.png

This module makes sure image which doesn't fit into required dimensions is
scaled down instead of cropped:

.. image:: https://bitbucket.org/hacbee/product_image_fit/raw/ac85475576638881da67c2457ecbdda84293985a/src/img/scale_example2.png

The logic applies to all modules which uses
standard OpenERP image library.

Padding of scaled images are of white color due to the fact that this module is
mostly relevant for product images (which when viewed in kanban view has white
background)