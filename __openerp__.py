# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.
{
    'name': 'Product Image Fit',
    'version': '0.1',
    'author': 'HacBee UAB',
    'category': 'Image',
    'website': 'http://www.hbee.eu',
    'summary': '',
    'description': """
Proper product image fit
==============================================================================
By default OpenERP scales product images by cropping parts which doesn't fit
into certain dimensions. This sometimes leads to malformed image, e.g. when you
have *narrow* image then top and bottom is chopped off leaving just image
center.

This module makes sure image which doesn't fit into required dimensions is
scaled down instead of cropped. The logic applies to all modules which uses
standard OpenERP image library.

Padding of scaled images are of white color due to the fact that this module is
mostly relevant for product images (which when viewed in kanban view has white
background)
""",
    'depends': [
    ],
    'data': [
    ],
    'installable': True,
    'application': False,
}
