# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

import io
import StringIO

from PIL import Image
from PIL import ImageOps

from openerp.tools import image


def image_fit(image, size):
    image.thumbnail(size, Image.ANTIALIAS)
    background = Image.new('RGBA', size, (255, 255, 255, 0))
    background.paste(
        image,
        ((size[0] - image.size[0]) / 2, (size[1] - image.size[1]) / 2)
    )
    return background


def better_image_resize_image(base64_source,
                              size=(1024, 1024),
                              encoding='base64',
                              filetype='PNG',
                              avoid_if_small=False):

    if not base64_source:
        return False
    if size == (None, None):
        return base64_source
    image_stream = io.BytesIO(base64_source.decode(encoding))
    image = Image.open(image_stream)

    asked_width, asked_height = size
    if asked_width is None:
        asked_width = int(image.size[0] * (float(asked_height) / image.size[1]))
    if asked_height is None:
        asked_height = int(image.size[1] * (float(asked_width) / image.size[0]))
    size = asked_width, asked_height

    # check image size: do not create a thumbnail if avoiding smaller images
    if avoid_if_small and image.size[0] <= size[0] and image.size[1] <= size[1]:
        return base64_source

    if image.size <> size:
        # If you need faster thumbnails you may use use Image.NEAREST
        #image = ImageOps.fit(image, size, Image.ANTIALIAS)
        # use our custom fit method!
        image = image_fit(image, size)
    if image.mode not in ["1", "L", "P", "RGB", "RGBA"]:
        image = image.convert("RGB")

    background_stream = StringIO.StringIO()
    image.save(background_stream, filetype)
    return background_stream.getvalue().encode(encoding)


image.image_resize_image = better_image_resize_image
